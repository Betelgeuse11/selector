# selector
## TODO
### functionality
- [ ] first
- [ ] all

### supported selectors
1. [ ] Tag
2. [ ] Classes
3. [ ] IDs
4. [ ] family 
    - [ ] inside
    - [ ] parent
    - [ ] next
    - [ ] before
5. [ ] attributes 
    - [ ] has
	- [ ] match
	- [ ] nomatch
	- [ ] starts
	- [ ] ends
	- [ ] contains
