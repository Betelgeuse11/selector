module selector

struct StepList {
mut:
	steps []Step
	idx int
}

fn (mut sl StepList) next() ?Step {
	if sl.idx >= sl.steps.len {
		return none
	}
	return sl.steps[sl.idx++]
}

// Step is a small wrapper for a StepType 
// and an optional associated value
struct Step {
	@type StepType [required]
	value ?string
}

// Describe what search factor should be added
// WIP: missing all attribute verifications
enum StepType {
	wildcard
	tag
	class
	id
	sibling
	child
	descendant
	neighbour
	preceded
	attribute
}
